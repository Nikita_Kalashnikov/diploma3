const express = require('express'); // подключаем библиотеку экспресс
const mongoose = require('mongoose'); // подключаем систему управления базой данных MongoDB
const applicationRoutes = require('./routes/application'); // подключаем файл с роутингом для заявок
const keys = require('./config/keys') // подключаем конфиг с ключами для соединения с базой данных.
const bodyParser = require('body-parser'); // подключаем body-parser для отправки данных.
const app = express(); // инициализируем приложение от фреймворка экспресс

// регистрируем роутинг
mongoose.connect(keys.mongoURI)
    .then((res) => {
        console.log('mongodb connected');
    })
    .catch(err => {
        console.log(err)
    });

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(require('cors')());
app.use('/api/application', applicationRoutes);

module.exports = app; // экспортируем наше приложение