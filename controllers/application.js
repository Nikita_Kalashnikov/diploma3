const Application = require('../models/Application');
const errorHandler = require('../utils/errorHandler');


module.exports.createApplication = async function(req, res) {
    const application = new Application({
        name: req.body.name,
        phone: req.body.phone,
        city: req.body.city,
        address: req.body.address,
        tariff: req.body.tariff
    })
    try {
        await application.save();
        res.status(201).json(application);
    } catch (e) {
        errorHandler(res, e);
    }
}