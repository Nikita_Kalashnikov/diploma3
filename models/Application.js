const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const applicationSchema = new Schema({
    name: {
        type: String
    },
    phone: {
        type: String
    },
    city: {
        type: String
    },
    address: {
        type: String
    },
    tariff: {
        type: String
    }
})

module.exports = mongoose.model('applications', applicationSchema);