import {Component, OnInit} from '@angular/core';
import {
  MatCarousel,
  MatCarouselComponent,
  MatCarouselSlide,
  MatCarouselSlideComponent
} from '@ngmodule/material-carousel';

@Component({
  selector: 'yasna-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
