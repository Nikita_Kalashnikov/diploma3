import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {cards, ICard} from "./cards";

@Component({
  selector: 'yasna-card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.scss']
})
export class CardContainerComponent implements OnInit {
  public cards: ICard[] = cards;
  public currentArrayLength: number = 6;
  @ViewChild('cardRef') cardRef: ElementRef;

  constructor() {
  }

  ngOnInit(): void {
  }

  public incrementLength(): void {
    this.currentArrayLength += 6;
  }

  public getLeftItems(): string {
    return this.cards.length - this.currentArrayLength >= 0 ? `(${this.cards.length - this.currentArrayLength})` : ''
  }

  public resetArrayLength():void {
    this.currentArrayLength = 6;
    this.cardRef.nativeElement.scrollIntoView({
      behavior: "smooth"
    });
  }

}
