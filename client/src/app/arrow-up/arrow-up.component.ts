import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'yasna-arrow-up',
  templateUrl: './arrow-up.component.html',
  styleUrls: ['./arrow-up.component.scss']
})
export class ArrowUpComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public scrollToTop(): void {
    document.querySelector('yasna-header').scrollIntoView({
      behavior: "smooth"
    })
  }
}
