import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ApplicationService} from "../application.service";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'yasna-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {
  public panelOpenState: boolean = false;
  public form: FormGroup;
  public tariffs = [
    {
      value: 'Ясна 35'
    },
    {
      value: 'Ясна 35 плюс'
    },
    {
      value: 'Ясна 35 Дом + Дача'
    },
    {
      value: 'Ясна 75'
    },
    {
      value: 'Ясна 75 плюс'
    },
    {
      value: 'Ясна 50 smart'
    },
    {
      value: 'Ясна 100'
    },
    {
      value: 'Ясна 100 комфорт'
    },
    {
      value: 'Ясна 100 smart'
    },
    {
      value: 'Ясна 200'
    },
    {
      value: 'Ясна Игровой'
    },
    {
      value: 'Ясна Игровой smart'
    },
    {
      value: 'Ясна Лайт'
    }
  ]

  constructor(
    private applicationService: ApplicationService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      phone: new FormControl(null, [Validators.required]),
      city: new FormControl(null, [Validators.required]),
      address: new FormControl(null, [Validators.required]),
      tariff: new FormControl(null, [Validators.required])
    })
  }

  public submit():void {
    this.applicationService.createApplication(this.form.value).
      subscribe(() => {
      this.dialog.open(DialogComponent)
    })
  }
}

@Component({
  selector: 'yasna-dialog',
  templateUrl: 'Dialog.component.html',
})
export class DialogComponent {}
