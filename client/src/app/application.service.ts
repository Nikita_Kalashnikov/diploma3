import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(
    private http: HttpClient
  ) {
  }

  public createApplication(application) {
    return this.http.post('/api/application', application)
  }
}
